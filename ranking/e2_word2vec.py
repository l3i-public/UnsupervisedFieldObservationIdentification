import glob
from os.path import basename
import operator

vecpath = 'C:\\Users\\rinterdo\\Documents\\Tools\\word2vec\\fr\\fr.bin'

#limit_entries = 500000
# Load Google's pre-trained Word2Vec model.
#model = gensim.models.KeyedVectors.load_word2vec_format(vecpath, binary=True)


import numpy as np
from scipy import spatial



def compareSubsets(lexicon, subsetdir, fout, model):
    with open(lexicon, 'r') as myfile:
        lex = myfile.read().replace('\n', ' ')

    sims = {}
    subs = glob.glob(subsetdir+"*")
    for file in subs:
        with open(file,'r') as f:
            cluster = ""
            for line in f:
                cluster+=" "+line.split(';')[1]+" "
            sim = calcSentenceSimilarity(model,lex,cluster)
            if not np.isnan(sim):
                sims[basename(file)]=sim
    with open(fout,'w') as fo:
        sorted_sims = sorted(sims.items(), key=operator.itemgetter(1),reverse=True)
        for entry in sorted_sims:
            fo.write("%s;%s\n" % (str(entry[0]),str(entry[1])))

def compareTweets(lexicon, subsetdir, subset_sims, perc, fout, model):

    with open(lexicon, 'r') as myfile:
        lex = myfile.read().replace('\n', ' ')

    sets_dict = {}

    with open(subset_sims, 'r') as sets_f:
        for line in sets_f:
            vals=line.split(';')
            sets_dict[vals[0]]=float(vals[1])
    p = np.percentile(list(sets_dict.values()),perc)

    sims = {}
    texts = {}

    for s in sets_dict:
        if sets_dict[s]>=p:
            with open("%s%s" % (subsetdir,s),'r') as curr_f:
                for line in curr_f:
                    vals = line.split(';')
                    texts[vals[0]]=vals[1]
                    sim = calcSentenceSimilarity(model,lex, vals[1])
                    if sim>0 and not np.isnan(sim):  # filter out tweets with zero similarity
                        sims[vals[0]]=sim

    with open(fout,'w') as fo:
        sorted_sims = sorted(sims.items(), key=operator.itemgetter(1), reverse=True)
        for entry in sorted_sims:
            id = entry[0]
            fo.write("%s;%s;%f\n"  % (id,texts[id].strip(),sims[id]))


def avg_feature_vector(sentence, model, num_features, index2word_set):
    words = sentence.split()
    feature_vec = np.zeros((num_features, ), dtype='float32')
    n_words = 0
    for word in words:
        if word in index2word_set:
            n_words += 1
            feature_vec = np.add(feature_vec, model[word])
    if (n_words > 0):
        feature_vec = np.divide(feature_vec, n_words)
    return feature_vec

def calcSentenceSimilarity(model,s1,s2):
    index2word_set = set(model.wv.index2word)
    s1_afv = avg_feature_vector(s1, model=model, num_features=300, index2word_set=index2word_set)
    s2_afv = avg_feature_vector(s2, model=model, num_features=300, index2word_set=index2word_set)
    sim = 1 - spatial.distance.cosine(s1_afv, s2_afv)
    return sim