from subprocess import Popen, PIPE, STDOUT
import os
import glob
from os.path import basename
import operator
import numpy as np

#esapath = "C:\\Users\\rinterdo\\Documents\\Tools\\ESA\\ESA_enwiki_index\\"
#esajar = "esa_en_tester_loc.jar"


def compareSubsets(lexicon, subsetdir, fout, esapath, esajar):
    with open(lexicon, 'r') as myfile:
        lex = myfile.read().replace('\n', ' ')

    sims = {}
    subs = glob.glob(subsetdir+"*")
    for file in subs:
            esa = calcEsa_SP(lex,file, esapath, esajar)
            if not np.isnan(esa):
                sims[basename(file)]=esa
    with open(fout,'w') as fo:
        sorted_sims = sorted(sims.items(), key=operator.itemgetter(1),reverse=True)
        for entry in sorted_sims:
            fo.write("%s;%s\n" % (str(entry[0]),str(entry[1])))

def compareTweets(lexicon, subsetdir, subset_sims, perc, fout,  esapath, esajar):

    with open(lexicon, 'r') as myfile:
        lex = myfile.read().replace('\n', ' ')

    sets_dict = {}

    with open(subset_sims, 'r') as sets_f:
        for line in sets_f:
            vals=line.split(';')
            sets_dict[vals[0]]=float(vals[1])
    p = np.percentile(list(sets_dict.values()),perc)

    sims = {}
    texts = {}

    for s in sets_dict:
        if sets_dict[s]>=p:
            with open("%s%s" % (subsetdir,s),'r') as curr_f:
                for line in curr_f:
                    vals = line.split(';')
                    texts[vals[0]]=vals[1]
                    esa = calcEsa_SS(lex, vals[1], esapath, esajar)
                    if esa>0 and not np.isnan(esa):  # filter out tweets with zero similarity
                        sims[vals[0]]=esa

    with open(fout,'w') as fo:
        sorted_sims = sorted(sims.items(), key=operator.itemgetter(1), reverse=True)
        for entry in sorted_sims:
            id = entry[0]
            fo.write("%s;%s;%f\n"  % (id,texts[id].strip(),sims[id]))


def calcEsa_SP(t1,t2, esapath, esajar):
    os.chdir("C:\\Users\\rinterdo\\Documents\\Tools\\ESA\\ESA_enwiki_index\\")

    p = Popen(['java', '-jar', esajar, esapath, '-s', t1,'-p',t2], stdout=PIPE, stderr=STDOUT)
    score = 0
    for line in p.stdout:
        s = line.strip().decode('UTF-8')
        score = float(s.strip())
    return score

def calcEsa_SS(t1,t2, esapath, esajar):
    os.chdir("C:\\Users\\rinterdo\\Documents\\Tools\\ESA\\ESA_enwiki_index\\")

    p = Popen(['java', '-jar', esajar, esapath, '-s', t1,'-s',t2], stdout=PIPE, stderr=STDOUT)
    score = 0
    for line in p.stdout:
        s = line.strip().decode('UTF-8')
        score = float(s.strip())
    return score

