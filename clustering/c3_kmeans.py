import os.path
from sklearn.feature_extraction.text import TfidfVectorizer

from sklearn.cluster import KMeans, MiniBatchKMeans

import sys
from time import time

import numpy as np

n_features = 10000

def readTweets(fin):
    texts = []
    with open(fin,'r') as fin:
        for line in fin:
            texts.append(line.split(';')[1])
    return texts

def writeSubsets(topic_pr,tw_path,dir):
    if not os.path.exists(dir):
        os.makedirs(dir)

    tdict = {}

    for index, row in enumerate(topic_pr):
        top = row.argmax()
        tdict[index] = top

    with open(tw_path, 'r') as file:
        for index, line in enumerate(file):
            topic = tdict[index]
            with open('%s%d_kmeans_topic_subsets.txt' % (dir,topic),'a+') as fw:
                fw.write(line)

def is_interactive():
    return not hasattr(sys.modules['__main__'], '__file__')


def kmeans(fname,true_k=10):
    texts = readTweets(fname)

    print("Extracting features from the training dataset using a sparse vectorizer")
    t0 = time()

    vectorizer = TfidfVectorizer(max_df=0.5, max_features=n_features,
                                 min_df=5, stop_words='english')
    X = vectorizer.fit_transform(texts)
    # #############################################################################
    # Do the actual clustering

    #km = MiniBatchKMeans(n_clusters=true_k, init='k-means++', n_init=1, init_size=1000, batch_size=1000, verbose=opts.verbose)

    km = KMeans(n_clusters=true_k, init='k-means++', max_iter=100, n_init=1)

    print("Clustering sparse data with %s" % km)
    km.fit(X)
    topic_pr = km.transform(X)
    print("done in %0.3fs." % (time() - t0))

    return topic_pr

    #print("Silhouette Coefficient: %0.3f" % metrics.silhouette_score(X, km.labels_, sample_size=1000))



    """
    print("Top terms per cluster:")

    order_centroids = km.cluster_centers_.argsort()[:, ::-1]

    terms = vectorizer.get_feature_names()
    for i in range(true_k):
        print("Cluster %d:" % i, end='')
        for ind in order_centroids[i, :10]:
            print(' %s' % terms[ind], end='')
        print()
    """

if __name__=='__main__':
    path = "C:\\Users\\rinterdo\\Documents\\CRISIS_Data\\Pipeline_test\\CrisisStream_sample.json.csv_preprocessed.csv"
    kmeans(path)
    exit()
    writeSubsets(kmeans(path),path,'C:\\Users\\rinterdo\\Documents\\CRISIS_Data\\Pipeline_test\\km_subs\\')