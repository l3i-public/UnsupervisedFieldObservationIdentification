# Author: Olivier Grisel <olivier.grisel@ensta.org>
#         Lars Buitinck
#         Chyi-Kwei Yau <chyikwei.yau@gmail.com>
# License: BSD 3 clause

import os.path
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.decomposition import NMF
from time import time

n_features = 10000

def print_top_words(model, feature_names, n_top_words):
    for topic_idx, topic in enumerate(model.components_):
        print("Topic #%d:" % topic_idx)
        print(" ".join([feature_names[i]
                        for i in topic.argsort()[:-n_top_words - 1:-1]]))
    print()


def readTweets(fin):
    texts = []
    with open(fin,'r') as fin:
        for line in fin:
            texts.append(line.split(';')[1])
    return texts

def nmfModel(fname,topics=10):
    # Load the 20 newsgroups dataset and vectorize it. We use a few heuristics
    # to filter out useless terms early on: the posts are stripped of headers,
    # footers and quoted replies, and common English words, words occurring in
    # only one document or in at least 95% of the documents are removed.
    # Use tf-idf features for NMF.

    texts = readTweets(fname)

    t0 = time()
    print("Extracting tf-idf features for NMF...")
    tfidf_vectorizer = TfidfVectorizer(max_df=0.5, min_df=5,
                                       max_features=n_features,
                                       stop_words='english')
    tfidf = tfidf_vectorizer.fit_transform(texts)
    # Fit the NMF model
    print("Fitting the NMF model with tf-idf features, "
          "n_features=%d..."
          % (n_features))
    nmf = NMF(n_components=topics, random_state=1,
              alpha=.1, l1_ratio=.5).fit(tfidf)

    topic_pr = nmf.transform(tfidf)
    print("done in %0.3fs." % (time() - t0))

    return topic_pr

def writeSubsets(topic_pr,tw_path,dir):
    if not os.path.exists(dir):
        os.makedirs(dir)

    tdict = {}

    for index, row in enumerate(topic_pr):
        top = row.argmax()
        tdict[index] = top

    with open(tw_path, 'r') as file:
        for index, line in enumerate(file):
            topic = tdict[index]
            with open('%s%d_nmf_topic_subsets.txt' % (dir,topic),'a+') as fw:
                fw.write(line)


if __name__=='__main__':
    path = "C:\\Users\\rinterdo\\Documents\\CRISIS_Data\\Pipeline_test\\CrisisStream_sample.json.csv_preprocessed.csv"
    writeSubsets(nmfModel(path),path,'C:\\Users\\rinterdo\\Documents\\CRISIS_Data\\Pipeline_test\\nmf_subs\\')