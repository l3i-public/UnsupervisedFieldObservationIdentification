from preprocessing import a_json_preprocessing, b_tweet_preprocessing
from clustering import c1_lda, c2_nmf, c3_kmeans
from ranking import e1_ESA, e2_word2vec
import gensim
from os.path import basename


if __name__=='__main__':

    # Pick one of the available clustering methods and semantic similarity measures
    clustering_algorithms = ['lda','nmf','kmeans']
    semantic_similarity_measures = ['ESA','word2vec']

    # 0 - lda
    # 1 - nmf
    # 2 - k-means
    clustering = clustering_algorithms[0]
    # 0 - esa
    # 1 - word2vec
    similarity = semantic_similarity_measures[1]

    # Choose if you want to perform the full pipeline or just some steps
    preprocessing_step = False
    clustering_step = False
    ranking_step = True

    #Parameters
    # I/O Paths
    # Path of the input file, containing a collection of Tweets in their original json format
    json_path = "C:\\Users\\rinterdo\\Documents\\CRISIS_Data\\Eleanor\\CrisisEleanorFr.json"
    # Path of the directory where to store the output files
    outdir = "C:\\Users\\rinterdo\\Documents\\CRISIS_Data\\Eleanor\\"
    # Path of the output file for the topic-subsets scores
    subset_sims = outdir + "subset_sims_%s_%s.csv" % (clustering,similarity)
    # Path of the output file storing the final tweet ranking
    tweet_sims = outdir + "tweet_sims_%s_%s.csv" % (clustering,similarity)


    # Preprocessing
    # -----------------------------
    # Path of the output file for json_preprocessing
    f1_name = "%s%s" % (outdir,basename(json_path)+".csv")
    # Path of the output file for tweet_preprocessing
    f2_name = "%s%s" % (outdir,basename(f1_name)+"_preprocessed.csv")
    # selected language (tweets in different languages will be filtered)
    lang = 'fr'
    # list of additional words to filter
    stoplist = ['RT']

    # Clustering
    # -----------------------------
    # Path of the stopwords file
    # pay attention: the stopwords file should match with the language selected for the preprocessing (default: english)
    # [stopword files for most common languages can be downloaded through the nltk python package
    stopwords = "%s%s" % (outdir, 'french')
    # Number of latent topics to discover with LDA
    num_topics = 100
    # Path of the output directory for the LDA topic modeling step
    outlda = outdir + "%s\\" % clustering
    # Path of the output directory for the Topic-based tweet subsets
    out_subsets = outdir + "subsets_%s\\" % clustering

    # Ranking
    # -----------------------------
    # Path of the Lexicon [list of words, one per line] to use for semantic similarity comparisons
    lexicon = "C:\\Users\\rinterdo\\Documents\\CRISIS_Data\\CrisisLexRec_FrenchGT.txt"
    # Top percentile of topic-subsets to analyze in the final step
    percentile = 90
    # -----------------------------
    # Only is similarity='ESA'
    # -----------------------------
    # Path of the ESA corpus files
    esapath = "C:\\Users\\rinterdo\\Documents\\Tools\\ESA\\ESA_frwiki_index\\"
    # Path of the ESA jar file
    esajar = "esa_en_tester_loc.jar"
    # -----------------------------
    # Only is similarity='word2vec'
    # -----------------------------
    # word2vec model path
    #w2v_path = 'C:\\Users\\rinterdo\\Documents\\Tools\\word2vec\\GoogleNews-vectors-negative300.bin'
    w2v_path = 'C:\\Users\\rinterdo\\Documents\\Tools\\word2vec\\fr\\fr.bin'
    # True if the model is trained with C code - i.e., GoogleNews - False otherwise (i.e., python trained models)
    cmodel = False
    # limit the number of entries to load in the model
    limit_entries = 500000





    # Script execution

    # Preprocessing
    if preprocessing_step:
        a_json_preprocessing.process(json_path, f1_name)
        b_tweet_preprocessing.preprocess(f1_name, f2_name, lang=lang, clean_from=stoplist)

    #Clustering
    if clustering_step:
        if clustering=='lda':
            # Clustering [LDA Topic Modeling + topic subsets]
            #c_lda_gensim.ldaProcessing(f2_name, stopwords, outlda, topics=num_topics)
            #d_topic_subsets.writeSubsets(outlda, f2_name, out_subsets)
            topic = c1_lda.ldaModel(f2_name, topics=num_topics)
            c1_lda.writeSubsets(topic, f2_name, out_subsets)
        elif clustering=='nmf':
            # Clustering [NMF Topic Modeling + topic subsets]
            topic = c2_nmf.nmfModel(f2_name, topics=num_topics)
            c2_nmf.writeSubsets(topic, f2_name, out_subsets)
        elif clustering=='kmeans':
            # Clustering [NMF Topic Modeling + topic subsets]
            topic = c3_kmeans.kmeans(f2_name, true_k=num_topics)
            c3_kmeans.writeSubsets(topic, f2_name, out_subsets)
        else:
            print("Error on clustering parameter: please choose one of the available values {'lda','nmf','kmeans'}")


    if ranking_step:
        if similarity=='ESA':
            # Ranking based on semantic similarity [ESA]
            e1_ESA.compareSubsets(lexicon, out_subsets, subset_sims,esapath,esajar)
            e1_ESA.compareTweets(lexicon, out_subsets, subset_sims, percentile, tweet_sims,esapath,esajar)
        elif similarity=='word2vec':
            # Ranking based on semantic similarity [word2vec]
            # Load Google's pre-trained Word2Vec model.
            if cmodel:
                model = gensim.models.KeyedVectors.load_word2vec_format(w2v_path, binary=True,limit=limit_entries)
            else:
                model = gensim.models.Word2Vec.load(w2v_path)
            e2_word2vec.compareSubsets(lexicon, out_subsets, subset_sims, model)
            e2_word2vec.compareTweets(lexicon, out_subsets, subset_sims, percentile, tweet_sims, model)
        else:
            print("Error on similarity parameter: please choose one of the available values {'ESA','word2vec'}")




