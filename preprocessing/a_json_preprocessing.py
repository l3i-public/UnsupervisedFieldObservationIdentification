import json
import os
import glob
import csv
import string

def process(fname,fout_name):
    #fout_name = "%s%s" % (outdir,basename(fname)+".csv")
    f = open(fname,'r')
    fout = open(fout_name,'w',encoding='utf-8')

    header = "tweet_id;text;time;retweet_count;favorite_count;coordinates;user_id;user_name;user_description;location;time_zone\n"
    fout.write(header)
    c = 0
    for line in f:
        try:
            parsed_json = json.loads(line)
            #print(parsed_json.keys())
            if 'id' in parsed_json.keys():
                c+=1
                tweet_id = parsed_json['id']
                #text = parsed_json['text'].encode('utf-8')
                text = str(parsed_json['text'])
                time = parsed_json['created_at']
                retweet_count = parsed_json['retweet_count']
                favorite_count = parsed_json['favorite_count']
                coordinates = parsed_json['coordinates']
                if coordinates == None:
                    coordinates=""
                us = parsed_json['user']
                user_id = us['id']
                username = us['screen_name']
                user_description = us['description']
                if user_description==None:
                    user_description=""
                else:
                    #user_description=user_description.encode('utf-8')
                    user_description = str(user_description)
                location = us['location']
                if location==None:
                    location=""
                else:
                    #location=location.encode('utf-8')
                    location = str(location)
                time_zone = us['time_zone']
                if time_zone==None:
                    time_zone=""
                else:
                    #time_zone=time_zone.encode('utf-8')
                    time_zone = str(time_zone)

                row  = "%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s" % (str(tweet_id),text,str(time),str(retweet_count),str(favorite_count),str(coordinates),str(user_id),str(username),user_description,location,time_zone)
                p_row = ''.join(filter(lambda x: x in string.printable, row))
                p_row = ''.join(p_row.splitlines())
                fout.write(p_row+"\n")
        except json.decoder.JSONDecodeError:
            print("JSONDecodeError on line %s" % line)
    fout.close()
    print(c," tweets processed")



    """
    us= parsed_json['user']
    if us['lang']=="fr":
        print(us['id'],us['screen_name'],us['followers_count'],us['friends_count'],us['statuses_count'],us['location'],us['time_zone'],us['lang'],us['created_at'])
        print(parsed_json['text'])
    """

def joincsv():
    header="tweet_id;text;time;retweet_count;favorite_count;coordinates;user_id;user_name;user_description;location;time_zone"
    os.chdir('D:\\CRISIS\\EngStream - csv\\')
    files = glob.glob("*.csv")
    df_out_filename = 'df_out.csv'
    write_headers = True
    with open(df_out_filename, 'wb') as fout:
        writer = csv.writer(fout)
        for filename in files:
            fin = open(filename, "rb")
            # replace needed for "NUL bytes" error
            reader =csv.reader(x.replace('\0', '') for x in fin)
            headers = reader.next()
            if write_headers:
                write_headers = False  # Only write headers once.
                writer.writerow(headers)
            writer.writerows(reader)  # Write all remaining rows.

def cleanCSV():
    interesting_files = glob.glob("*.csv")
    k=0
    for file in interesting_files:
        fin = open(file,'r')
        for line in fin:
           c= line.count(';')
           if c!=11:
               k+=1
    print(k)




if __name__=='__main__':

    path = "C:\\Users\\rinterdo\\Documents\\crypto\\CryptoStream_120212018_2.json"
    f1_name = "C:\\Users\\rinterdo\\Documents\\crypto\\CryptoStream_120212018_2.csv"

    process(path,f1_name)