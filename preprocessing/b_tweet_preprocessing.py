import preprocessor as p
from langdetect import detect
from langdetect import lang_detect_exception
import string
import glob
from os.path import basename


# Clean from
# - url, emoji, mention
# - punctuation
# - filter by language selection
# - clean from additional words [list "clean_from"]
# - clear from null and empty strings
def preprocess(fpath,fout_p,lang='en',clean_from=['RT'],filter_duplicates=True):
    p.set_options(p.OPT.URL, p.OPT.EMOJI, p.OPT.MENTION)
    fo = open(fout_p, 'w')
    # fo.write("id;text\n")  # no header  [for better lda processing]
    texts = set()  # for duplicates filtering
    with open(fpath, 'r') as file:
        next(file)  # skip header
        for line in file:
            vals = line.split(';')
            try:
                if detect(vals[1]) == lang:
                    text = p.clean(vals[1])  # clean from url, emoji, mention
                    translator = str.maketrans('', '', string.punctuation)
                    text = text.translate(translator).strip()  # remove punctuation
                    for word in clean_from:  # clean from additional unwanted words
                        text = text.replace(word, '')
                    if text != '' and text is not None:  # clear from null and empty strings
                        if filter_duplicates and (text in texts):
                            continue
                        else:
                            fo.write("%s;%s\n" % (vals[0], text))
                        if filter_duplicates:
                            texts.add(text)
            except lang_detect_exception.LangDetectException:
                print("Lang Detect exception for: ", vals[1])

    fo.close()

if __name__=='__main__':
    files = glob.glob("./*.csv")

    for f in files:
        fo1 = "%s_p.csv" % basename(f)
        fo2 = "%s_p_noDuplicates.csv" % basename(fo1)
        preprocess(f,fo1)
